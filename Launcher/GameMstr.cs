﻿//#define TEST
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using Board;
using Client;
using Server;
using Message.Communication;
using Newtonsoft.Json;

namespace Launcher
{

    public class GameMaster : Client.Client
    {
        //<old>
        private List<Tuple<string, Location>> PlayerLocations = new List<Tuple<string, Location>>();
        private List<Tuple<ulong, string>> PlayerIdsWithGuids = new List<Tuple<ulong, string>>();


        public List<Tuple<int, Tuple<int, int>>> ItemsLocations = new List<Tuple<int, Tuple<int, int>>>();

        public Dictionary<int, bool> isSham = new Dictionary<int, bool>();
        private Dictionary<int, int> PlayerWithPiece = new Dictionary<int, int>();
        public int goalTasks = 0;
        public int goalTasksBlue;
        public int goalTasksRed;
        // </old>

        private readonly String logFilePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\" + "logs" + "\\" + DateTime.Now.ToString("yyyy-MM-ddTHH-mm");


        private int redSpotsAvailable = 0, blueSpotsAvailable = 0;
        private Player redLeader = null, blueLeader = null;
        private List<Player> PlayersList = new List<Player>();
        private int gameId = 0;
        private Object monitor = new Object();
        public GameMaster(int NoOfBluePlayers, int NoOfRedPlayers, string gameName, int numberOfItems, int shams, int goalTasks, int goalAreaHeight, int boardWidth, int boardHeight)
        {
            redSpotsAvailable = NoOfRedPlayers;
            blueSpotsAvailable = NoOfBluePlayers;

            GameInfo gameInfo = new GameInfo();

            this.goalTasks = goalTasks;
            goalTasksBlue = goalTasks / 2;
            goalTasksRed = goalTasks / 2;
            RegisterGame rg = new RegisterGame();

            gameInfo.blueTeamPlayers = (ulong)NoOfBluePlayers;
            gameInfo.redTeamPlayers = (ulong)NoOfRedPlayers;
            gameInfo.gameName = gameName;
            rg.NewGameInfo = gameInfo;
            SendPacket(rg);

            Directory.CreateDirectory("logs");
            Board = new BoardGame(boardWidth, boardHeight, goalAreaHeight);
            InitializeItems(numberOfItems, shams, goalTasks);
            //RegisterToServerAndGetId(ClientType.GameMaster, maxNoOfPlayers);
        }

        public override void HandleReceivePacket(Object oo)
        {
            lock (monitor)
            {
                if (oo is JoinGame)     // a player wants to join a game
                    JoinGameMethod(oo);
                else if (oo is ConfirmGameRegistration)
                    ConfirmGameRegistrationMethod(oo);
                else if (oo is PlayerDisconnected pd)
                {
                    var playerToRemove = PlayersList.Find(p => p.id == pd.playerId);
                    LogRequest(oo, PlayerIdsWithGuids.Find(match => match.Item1 == pd.playerId).Item2);

                    if (playerToRemove.team == TeamColour.blue)
                    {
                        blueSpotsAvailable++;
                        if (playerToRemove.role == PlayerRole.leader)
                            blueLeader = PlayersList.Find(p => p.team == TeamColour.blue);
                    }
                    else
                    {
                        redSpotsAvailable++;
                        if (playerToRemove.role == PlayerRole.leader)
                            redLeader = PlayersList.Find(p => p.team == TeamColour.red);

                    }

                    Console.WriteLine(PlayersList.Remove(playerToRemove) ? "player removed" : "error removing player");
                }
                else if (oo is Move move)
                {
                    LogRequest(oo, move.playerGuid);
                    Thread.Sleep(100);
                    ValidateMove(move);
                }
                else if (oo is PlacePiece placePiece)
                {
                    LogRequest(oo, placePiece.playerGuid);
                    Thread.Sleep(100);
                    var playerDoingActionGuidIdTuple = PlayerIdsWithGuids.Find(p => p.Item2.Equals(placePiece.playerGuid));
                    var playerDoingActionId = playerDoingActionGuidIdTuple.Item1;
                    var player = PlayersList.Find(p => p.id == playerDoingActionId);
                    var playerLocation = player.playerLocation;
                    var piece = PlayerWithPiece[(int)playerDoingActionId];
                    //TODO: check if player is carrying a piece
                    if (Board.inGoalArea((int)playerLocation.x, (int)playerLocation.y) && PlayerWithPiece.ContainsKey((int)playerDoingActionId)) // placed on goal area
                    {

                        //field is a goal field in goal area
                        GoalField goalField = new GoalField
                        {
                            x = playerLocation.x,
                            y = playerLocation.y,
                            playerId = playerDoingActionId,
                            team = player.team,
                            timestamp = DateTime.Now,
                        };

                        if (!isSham[piece])
                        {
                            var field = Board.Board[(int)playerLocation.x, (int)playerLocation.y];
                            if (field.goalFieldType == GoalFieldType.goal)
                            {
                                goalField.type = GoalFieldType.goal;
                                Console.WriteLine("TASK AT " + playerLocation.x + "x" + playerLocation.y + " COMPLETED!!");
                                Board.Board[(int)playerLocation.x, (int)playerLocation.y].Content = FieldContent.Empty;
                                Board.Board[(int)playerLocation.x, (int)playerLocation.y].goalFieldType = GoalFieldType.nongoal;    // field becomes non goal
                                goalTasks--;
                                if (player.team == TeamColour.blue)
                                    goalTasksBlue--;
                                else
                                    goalTasksRed--;
                                if(goalTasksRed > 0 && goalTasksBlue > 0)
                                    AddNewPiece();
                            }
                            else if (field.goalFieldType == GoalFieldType.nongoal)
                            {
                                Console.WriteLine("ITS NON GOAL");
                                goalField.type = GoalFieldType.nongoal;
                                AddNewPiece();
                            }
                        }
                        else
                        {
                            goalField.type = GoalFieldType.unknown;
                            AddNewPiece();
                        }
                        DestroyPiece(piece);
                        Data data = new Data
                        {
                            gameFinished = player.team == TeamColour.blue ? goalTasksBlue <= 0 : goalTasksRed <= 0,
                            playerId = playerDoingActionId,
                            GoalFields = new[] { goalField }
                        };
                        SendPacket(data);

                        //if game finished, notify other player
                        if (player.team == TeamColour.blue ? goalTasksBlue <= 0 : goalTasksRed <= 0)
                        {
                            foreach (Player p in PlayersList)
                            {
                                Data end = new Data();
                               
                                end.playerId = p.id;
                                end.gameFinished = true;
                                SendPacket(end);
                                Thread.Sleep(50);
                            }
                        }
                    }
                    else if (PlayerWithPiece.ContainsKey((int)playerDoingActionId)) // placed on task area
                    {
                        Data data = new Data
                        {
                            gameFinished = player.team == TeamColour.blue ? goalTasksBlue <= 0 : goalTasksRed <= 0,
                            playerId = playerDoingActionId,
                        };
                        if (hasItem((int)playerLocation.x, (int)playerLocation.y))
                        {
                            data.Pieces = new Piece[] { new Piece { playerId = playerDoingActionId, timestamp = DateTime.Now, id = (ulong)piece, type = PieceType.unknown } };
                        }
                        else
                        {
                            PlayerWithPiece.Remove((int)playerDoingActionId);
                            DestroyPiece(piece);
                        }
                        SendPacket(data);
                        AddNewPiece();
                    }
                    // does not have piece
                    else
                    {
                        Console.WriteLine("Player tried place piece without having a piece");
                    }
                }
                else if (oo is PickUpPiece pickUp)
                {
                    LogRequest(oo, pickUp.playerGuid);
                    Thread.Sleep(100);
                    var playerGuidIdTuple = PlayerIdsWithGuids.Find(t => t.Item2.Equals(pickUp.playerGuid));
                    var playerId = playerGuidIdTuple.Item1;
                    var player = PlayersList.Find(p => p.id == playerId);
                    var playerLocation = player.playerLocation;

                    if (hasItem((int)playerLocation.x, (int)playerLocation.y) && !PlayerWithPiece.ContainsKey((int)playerId))
                    {
                        Random random = new Random();
                        var item = ItemsLocations.Find(i => i.Item2.Item1 == playerLocation.x && i.Item2.Item2 == playerLocation.y);
                        //if field contains sham piece or real piece we can pick it
                        Piece piece = new Piece
                        {
                            id = (ulong)item.Item1,
                            type = PieceType.unknown,
                            timestamp = DateTime.Now,
                            playerId = playerId
                        };
                        Data data = new Data
                        {
                            playerId = playerId,
                            gameFinished = player.team == TeamColour.blue ? goalTasksBlue <= 0 : goalTasksRed <= 0,
                            Pieces = new[] { piece }
                        };
                        ItemsLocations.Remove(item);
                        Console.WriteLine("Player " + playerId + " picked up a piece");
                        PlayerWithPiece.Add((int)playerId, item.Item1);
                        SendPacket(data);
                    }
                    else
                    {
                        Console.WriteLine("Player tried illegal operation to pick up a piece from a field which does not contain one");
                    }
                }
                else if (oo is TestPiece testPiece)
                {
                    LogRequest(oo, testPiece.playerGuid);
                    Thread.Sleep(500);
                    var playerGuidIdTuple = PlayerIdsWithGuids.Find(t => t.Item2.Equals(testPiece.playerGuid));
                    var playerId = playerGuidIdTuple.Item1;
                    var player = PlayersList.Find(p => p.id == playerId);
                    var playerLocation = player.playerLocation;
                    int pieceId = PlayerWithPiece[(int)playerId];
                    Data data = new Data
                    {
                        playerId = playerId,
                        gameFinished = player.team == TeamColour.blue ? goalTasksBlue <= 0 : goalTasksRed <= 0,
                        Pieces = new Piece[] { new Piece { id = (ulong)pieceId, playerId = playerId, timestamp = DateTime.Now, type = isSham[pieceId] ? PieceType.sham : PieceType.normal } }
                    };
                    SendPacket(data);
                }
                else if (oo is Discover d)
                {
                    LogRequest(oo, d.playerGuid);
                    Thread.Sleep(450);
                    Discover(d);
                }
                //else if(oo is Data data)

                //TODO - handle something received from another entit
            }

        }

        private void AddNewPiece()
        {
            Tuple<int, int> loc = null;
            List<Tuple<int, int>> available = new List<Tuple<int, int>>();
            for (int i = 0; i < Board.Width; i++)
            {
                for (int j = Board.GoalAreaHeight; j < Board.Height - Board.GoalAreaHeight; j++)
                {
                    if (hasItem(i, j) || hasPlayer(i, j)) continue;
                    available.Add(new Tuple<int, int>(i, j));
                }
            }

            Random r = new Random();
            int idx = r.Next(available.Count);
            loc = available[idx];
            ItemsLocations.Add(new Tuple<int, Tuple<int, int>>(isSham.Keys.Max() + 1, loc));
            available.RemoveAt(idx);
            Console.WriteLine($"Item added to {loc.Item1}x{loc.Item2}");
            isSham.Add(isSham.Keys.Max()+1, idx%2==0);
        }

        private void LogRequest(Object oo, String PID)
        {
            using (FileStream fs = new FileStream(logFilePath + PID + "_log.txt", FileMode.Append))
            {
                StreamWriter sw = new StreamWriter(fs);
                sw.WriteAsync(DateTime.Now.ToString("HH:mm:ss"));
                sw.WriteLineAsync(" - GM recieved packet of type: " + oo.ToString().Split('.')[2]);
                sw.WriteLineAsync(JsonConvert.SerializeObject(oo));
                sw.WriteLineAsync("-----------------------------");
                sw.Close();
            }
        }

        public void DestroyPiece(int id)
        {
            ItemsLocations.RemoveAll(match => match.Item1 == id);
            isSham.Remove(id);
            foreach (var i in PlayerWithPiece.Where(match => match.Value == id).ToList())
            {
                PlayerWithPiece.Remove(i.Key);
            }
        }
        private int getPlayerId(string guid)
        {
            return (int)PlayerIdsWithGuids.Find(match => match.Item2 == guid).Item1;
        }
        private void ValidateMove(Move move)
        {

            for (int i = 0; i < PlayerLocations.Count; ++i)
            {
                for (int j = 0; j < PlayerLocations.Count; ++j)
                {
                    if (i == j) continue;
                    bool r = PlayerLocations[i].Item2.x == PlayerLocations[j].Item2.x && PlayerLocations[i].Item2.y == PlayerLocations[j].Item2.y;
                    System.Diagnostics.Debug.Assert(!r, "TWO PLAYERS AT THE SAME CELL!");
                }
            }
            Console.WriteLine("Validate move for player :" + move.playerGuid);
            Location currentLocation = new Location();
            currentLocation.x = (uint)PlayerLocations.Find(x => x.Item1 == move.playerGuid).Item2.x;
            currentLocation.y = (uint)PlayerLocations.Find(x => x.Item1 == move.playerGuid).Item2.y;
            Location newLocation = new Location();
            newLocation.x = (uint)currentLocation.x;
            newLocation.y = (uint)currentLocation.y;

            if (move.direction == MoveType.left)
                newLocation.x--;
            else if (move.direction == MoveType.right)
                newLocation.x++;
            else if (move.direction == MoveType.up)
                newLocation.y++;
            else if (move.direction == MoveType.down)
                newLocation.y--;

            //check it it's an edge
            if (Board.InRange((int)newLocation.x, (int)newLocation.y))
            {
                int id = getPlayerId(move.playerGuid);
                var player = PlayersList.Find(match => match.id == (ulong)id);

                //check if occupied or entered goal area of opposite team
                if (PlayerLocations.Find(i => i.Item2.x == newLocation.x && i.Item2.y == newLocation.y) != null // occupied
                    || player.team == TeamColour.blue && Board.RedGoalArea((int)newLocation.x, (int)newLocation.y)  // blue enters red area
                    || player.team == TeamColour.red && Board.BlueGoalArea((int)newLocation.x, (int)newLocation.y)) // red enters blue area
                {

                    Data moveData = new Data();

                    moveData.PlayerLocation = currentLocation;
                    moveData.playerId = PlayerIdsWithGuids.Find(x => x.Item2 == move.playerGuid).Item1;
                    moveData.gameFinished = player.team == TeamColour.blue ? goalTasksBlue <= 0 : goalTasksRed <= 0;

                    SendPacket(moveData);
                }
                else
                {

                    Data moveData = new Data();
                    moveData.PlayerLocation = newLocation;
                    moveData.playerId = PlayerIdsWithGuids.Find(x => x.Item2 == move.playerGuid).Item1;
                    moveData.gameFinished = player.team == TeamColour.blue ? goalTasksBlue <= 0 : goalTasksRed <= 0;


                    if (!Board.inGoalArea((int)newLocation.x, (int)newLocation.y))
                    {
                        moveData.TaskFields = new TaskField[1];
                        moveData.TaskFields[0] = new TaskField()
                        {
                            distanceToPiece = manHattanDistance((int)newLocation.x, (int)newLocation.y),
                            x = newLocation.x,
                            y = newLocation.y,
                            timestamp = DateTime.Now
                        };
                    }
                    else
                    {
                        moveData.GoalFields = new GoalField[1];
                        moveData.GoalFields[0] = new GoalField
                        {
                            x = newLocation.x,
                            y = newLocation.y,
                            timestamp = DateTime.Now,
                            type = GoalFieldType.unknown
                        };
                    }


                    int idx = PlayerLocations.FindIndex(match => match.Item1 == move.playerGuid);
                    PlayerLocations[idx] = new Tuple<string, Location>(move.playerGuid, newLocation);

                    idx = PlayersList.FindIndex(match => match.id == (ulong)id);
                    PlayersList[idx].playerLocation = newLocation;

                    SendPacket(moveData);
                }


            }
            else
            {
                Data moveData = new Data();
                moveData.playerId = PlayerIdsWithGuids.Find(x => x.Item2 == move.playerGuid).Item1;
                moveData.PlayerLocation = currentLocation;

                var player = PlayersList.Find(match => match.id == moveData.playerId);
                moveData.gameFinished = player.team == TeamColour.blue ? goalTasksBlue <= 0 : goalTasksRed <= 0;
            }

        }
        private void ConfirmGameRegistrationMethod(Object oo)
        {
            ConfirmGameRegistration c = (ConfirmGameRegistration)oo;
            SetId((int)c.gameId);
            gameId = (int)c.gameId;
            Console.WriteLine("ID is set for game master : " + Id);
        }
        private void JoinGameMethod(Object oo)
        {

            Console.WriteLine("master received join game");
            JoinGame jg = (JoinGame)oo;
            if (redSpotsAvailable == 0 && blueSpotsAvailable == 0)
            {
                RejectJoiningGame rj = new RejectJoiningGame();
                rj.gameName = jg.gameName;
                rj.playerId = jg.playerid;
                Console.WriteLine("NO SPOTS!");
                SendPacket(rj);
            }
            else
            {
                if ((jg.preferredTeam == TeamColour.blue && blueSpotsAvailable > 0) || (jg.preferredTeam == TeamColour.red && redSpotsAvailable == 0))
                {
                    Console.WriteLine("blue choosed");
                    blueSpotsAvailable--;
                    if (jg.preferredRole == PlayerRole.leader)
                    {
                        addPlayer(TeamColour.blue, blueLeader == null ? PlayerRole.leader : PlayerRole.member, (int)jg.playerid, jg);
                    }
                    else
                    {
                        addPlayer(TeamColour.blue, PlayerRole.member, (int)jg.playerid, jg);
                    }
                }
                else
                {
                    Console.WriteLine("red choosed");
                    redSpotsAvailable--;
                    if (jg.preferredRole == PlayerRole.leader)
                    {
                        addPlayer(TeamColour.red, redLeader == null ? PlayerRole.leader : PlayerRole.member, (int)jg.playerid, jg);
                    }
                    else
                    {
                        addPlayer(TeamColour.red, PlayerRole.member, (int)jg.playerid, jg);
                    }
                }
            }
            if (redSpotsAvailable == 0 && blueSpotsAvailable == 0)  // send the game info to the players
            {
                foreach (Player p in PlayersList)
                {
                    Game g = new Game();
                    g.Board = new GameBoard();
                    g.Board.goalsHeight = (uint)Board.GoalAreaHeight;
                    g.Board.tasksHeight = (uint)(Board.Height - 2 * Board.GoalAreaHeight);
                    g.Board.width = (uint)Board.Width;
                    g.playerId = p.id;
                    g.PlayerLocation = p.playerLocation;
                    g.Players = PlayersList;
                    SendPacket(g);
                    Thread.Sleep(50);
                }

                GameStarted start = new GameStarted();
                start.gameId = (ulong)gameId;
                SendPacket(start);

            }


        }
        private void Discover(Discover discover)
        {
            TaskField[] taskfields = new TaskField[9];

            var location = findPlayerLocation(discover.playerGuid);
            int a = 0;
            for (int i = 1; i < 4; i++)
            {
                for (int j = 1; j < 4; j++)
                {
                    var newX = location.x + i - 2;
                    var newY = location.y + j - 2;
                    if (location.x + i - 2 < 0
                       || location.y + j - 2 < 0
                       || location.x + i - 2 > Board.Width - 1
                       || location.y + j - 2 > Board.Height - 1
                       || Board.inGoalArea((int)newX, (int)newY))   // its not a taskfield if it is a goal field
                        continue;

                    var tf = new TaskField
                    {
                        x = (uint)(location.x + i - 2),
                        y = (uint)(location.y + j - 2),
                        distanceToPiece = int.MaxValue,
                    };
                    if (hasPlayer(tf.x, tf.y))
                        tf.playerId = isOccupiedPlayer(tf.x, tf.y).id;

                    taskfields[a++] = tf;
                }
            }

            foreach (var taskfield in taskfields)
            {
                if (taskfield == null) continue;
                var distance = manHattanDistance((int)taskfield.x, (int)taskfield.y);
                if (taskfield.distanceToPiece <= distance) continue;

                taskfield.distanceToPiece = (int)distance;
                taskfield.timestamp = DateTime.Now;
            }

            ulong playerId = (ulong)getPlayerId(discover.playerGuid);
            var player = PlayersList.Find(match => match.id == playerId);
            Data data = new Data
            {
                playerId = (ulong)getPlayerId(discover.playerGuid),
                TaskFields = taskfields,
                gameFinished = player.team == TeamColour.blue ? goalTasksBlue <= 0 : goalTasksRed <= 0
            };

            SendPacket(data);
        }

        public string RandomString(int length)
        {
            Random random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            string ret;
            do
            {
                ret = new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
            } while (PlayerLocations.Find(match => match.Item1 == ret) != null);

            return ret;
        }
        private void addPlayer(TeamColour color, PlayerRole role, int id, JoinGame jg)
        {
            Player p = new Player();
            p.role = role;
            p.team = color;
            p.id = (ulong)id;

            if (role == PlayerRole.leader)
            {
                if (color == TeamColour.blue)
                    blueLeader = p;
                else
                    redLeader = p;
            }

            // get the available cells
            List<Tuple<int, int>> available = new List<Tuple<int, int>>();
            for (int i = 0; i < Board.Width; i++)
            {
                for (int j = Board.GoalAreaHeight; j < Board.Height - Board.GoalAreaHeight; j++)
                {
                    if (hasItem(i, j) || hasPlayer(i, j)) continue;
                    available.Add(new Tuple<int, int>(i, j));
                }
            }
            // give the player a position 
            Random rand = new Random();
            int idx = rand.Next(available.Count);
            int x = available[idx].Item1;
            int y = available[idx].Item2;

            PlayersList.Add(p);


            p.playerLocation = new Location();
            p.playerLocation.x = (uint)x;
            p.playerLocation.y = (uint)y;


            ConfirmJoiningGame cg = new ConfirmJoiningGame();
            cg.gameId = (ulong)gameId;
            cg.PlayerDefinition = p;
            cg.playerId = p.id;
            cg.privateGuid = RandomString(10);
            Console.WriteLine("PLAYERGUID = " + cg.privateGuid + " for id = " + cg.playerId);
            PlayerLocations.Add(new Tuple<string, Location>(cg.privateGuid, p.playerLocation));
            PlayerIdsWithGuids.Add(new Tuple<ulong, string>(cg.playerId, cg.privateGuid));
            LogRequest(jg, cg.privateGuid);

            SendPacket(cg);
        }

        private void InitializeItems(int numberOfItems, int shams, int goalTasks)
        {
            Thread.Sleep(1000);
            Random r = new Random();
            Tuple<int, int> loc = null;

            // get the available cells
            List<Tuple<int, int>> available = new List<Tuple<int, int>>();
            for (int i = 0; i < Board.Width; i++)
            {
                for (int j = Board.GoalAreaHeight; j < Board.Height - Board.GoalAreaHeight; j++)
                {
                    if (hasItem(i, j) || hasPlayer(i, j)) continue;
                    available.Add(new Tuple<int, int>(i, j));
                }
            }

            for (int i = 0; i < numberOfItems + shams; i++)
            {
                int idx = r.Next(available.Count);
                loc = available[idx];
                ItemsLocations.Add(new Tuple<int, Tuple<int, int>>(i + 1, loc));
                available.RemoveAt(idx);
                Console.WriteLine($"Item added to {loc.Item1}x{loc.Item2}");
                isSham.Add(i + 1, i >= numberOfItems);
            }

            // intialize goalTasks
            for (int i = 0; i < Board.Width; ++i)
            {
                for (int j = 0; j < Board.GoalAreaHeight; ++j)
                {
                    Board.Board[i, j].goalFieldType = GoalFieldType.nongoal;
                }
                for (int j = Board.Height - Board.GoalAreaHeight; j < Board.Height; ++j)
                {
                    Board.Board[i, j].goalFieldType = GoalFieldType.nongoal;
                }
            }
#if TEST

            for (int i = 0; i < Board.Width; i++)
            {
                for (int j = 0; j < Board.GoalAreaHeight; ++j)
                {
                    Board.Board[i, j].Content = FieldContent.GoalTask;
                    Board.Board[i, j].goalFieldType = GoalFieldType.goal;
                    Console.WriteLine("GOAL TASK ADDED TO " + i + "x" + j);
                }
                for (int j = Board.Height - Board.GoalAreaHeight; j < Board.Height; ++j)
                {
                    Board.Board[i, j].Content = FieldContent.GoalTask;
                    Board.Board[i, j].goalFieldType = GoalFieldType.goal;
                    Console.WriteLine("GOAL TASK ADDED TO " + i + "x" + j);
                }
            }
#endif
            for (int i = 0; i < goalTasks; ++i)
            {
                int x, y;
                do
                {
                    x = r.Next(Board.Width);
                    y = i < goalTasks / 2 ? r.Next(Board.GoalAreaHeight) : r.Next(Board.Height - Board.GoalAreaHeight, Board.Height);
                } while (Board.Board[x, y].Content == FieldContent.GoalTask);
                Console.WriteLine("GOAL TASK ADDED TO " + x + "x" + y);
                Board.Board[x, y].Content = FieldContent.GoalTask;
                Board.Board[x, y].goalFieldType = GoalFieldType.goal;
            }
        }

        public bool hasPlayer(int x, int y)
        {
            return PlayersList.Exists(i => i.playerLocation.x == x && i.playerLocation.y == y);
        }
        public bool hasPlayer(ulong x, ulong y)
        {
            return hasPlayer((int)x, (int)y);
        }
        public Player isOccupiedPlayer(ulong x, ulong y)
        {
            return PlayersList.Find(i => i.playerLocation.x == x && i.playerLocation.y == y);
        }
        public bool hasItem(int x, int y)
        {
            return ItemsLocations.Exists(i => i.Item2.Item1 == x && i.Item2.Item2 == y);
        }
        public int manHattanDistance(int x, int y)
        {
            int distance = int.MaxValue;
            foreach (var i in ItemsLocations)
            {
                distance = Math.Min(distance, Math.Abs(x - i.Item2.Item1) + Math.Abs(y - i.Item2.Item2));
            }
            return distance == int.MaxValue ? -1 : distance;
        }
        public Location findPlayerLocation(string guid)
        {
            return PlayerLocations.Find(match => match.Item1 == guid).Item2;
        }

        public void SendDisconnect()
        {
            GameMasterDisconnected gmd = new GameMasterDisconnected
            {
                gameId = (ulong)gameId,
                Players = PlayersList
            };

            SendPacket(gmd);

            Console.WriteLine("Game Master is terminating");
            Thread.Sleep(1000);
        }
    }
    public class GameMstr
    {
        public static Application GameWindowApplication;
        [STAThread]
        private static void Main(string[] args)
        {
            // TODO change all these to configuration file

            int NoOfBluePlayers, NoOfRedPlayers, goalAreaHeight, boardWidth, boardHeight, numberOfItems, shams, goalTasks;
            string gameName;

            if (args.Length != 0)
            {
                NoOfBluePlayers = int.Parse(args[0]);
                NoOfRedPlayers = int.Parse(args[1]);
                Console.WriteLine("blue = " + NoOfBluePlayers + ", red = " + NoOfRedPlayers);
                gameName = args[2];
                numberOfItems = int.Parse(args[3]);
                shams = int.Parse(args[4]);
                goalTasks = int.Parse(args[5]);
                boardWidth = int.Parse(args[6]);
                boardHeight = int.Parse(args[7]);
                goalAreaHeight = int.Parse(args[8]);
                GameMaster master = new GameMaster(NoOfBluePlayers, NoOfRedPlayers, gameName, numberOfItems, shams, goalTasks, goalAreaHeight, boardWidth, boardHeight);
                while (!(Console.KeyAvailable && Console.ReadKey().Key == ConsoleKey.Escape)) ;
                master.SendDisconnect();
            }
            else
            {
                Console.WriteLine("Number of blue players:");

                while (!int.TryParse(Console.ReadLine(), out NoOfBluePlayers) || !(NoOfBluePlayers > 0))
                    Console.WriteLine("\t Please enter an integer");

                Console.WriteLine("Number of red players:");

                while (!int.TryParse(Console.ReadLine(), out NoOfRedPlayers) || !(NoOfRedPlayers > 0))
                    Console.WriteLine("\t Please enter an integer");

                Console.WriteLine("Enter the game name : ");

                gameName = Console.ReadLine();


                Console.WriteLine("Number of items:");

                while (!int.TryParse(Console.ReadLine(), out numberOfItems) || !(numberOfItems >= 0))
                    Console.WriteLine("\t Please enter an integer");

                Console.WriteLine("Number of sham pieces:");

                while (!int.TryParse(Console.ReadLine(), out shams) || !(shams >= 0))
                    Console.WriteLine("\t Please enter an integer");

                Console.WriteLine("Goal Tasks:");

                while (!int.TryParse(Console.ReadLine(), out goalTasks) || !(goalTasks > 0))
                    Console.WriteLine("\t Please enter an integer");


                Console.WriteLine("Board width:");

                while (!int.TryParse(Console.ReadLine(), out boardWidth) || !(boardWidth > 0))
                    Console.WriteLine("\t Please enter an integer");

                Console.WriteLine("Board height:");

                while (!int.TryParse(Console.ReadLine(), out boardHeight) || !(boardHeight > 0))
                    Console.WriteLine("\t Please enter an integer");

                Console.WriteLine("Goal area height:");

                while (!int.TryParse(Console.ReadLine(), out goalAreaHeight) || !(goalAreaHeight > 0))
                    Console.WriteLine("\t Please enter an integer");



                if (goalAreaHeight > boardHeight / 2)
                {
                    Console.WriteLine(
                        " \tThe height of goal area has to be smaller than half of the board height. \n \t Please enter valid height");
                    while (!int.TryParse(Console.ReadLine(), out goalAreaHeight))
                        Console.WriteLine("\t Please enter an integer");
                }
                // create the master
                GameMaster master = new GameMaster(NoOfBluePlayers, NoOfRedPlayers, gameName, numberOfItems, shams, goalTasks, goalAreaHeight, boardWidth, boardHeight);
                while (!(Console.KeyAvailable && Console.ReadKey().Key == ConsoleKey.Escape)) ;
                master.SendDisconnect();
            }


        }



        private static void DispatchToApp(Action action)
        {
            GameWindowApplication.Dispatcher.Invoke(action);
        }
    }
}
