﻿//#define TEST
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using Client;
using Message.Communication;

namespace Server
{

    // State object for reading client data asynchronously
    public class StateObject
    {
        // Client  socket.
        public Socket WorkSocket = null;

        // Size of receive buffer.
        public const int BufferSize = ServerConstants.ServerBufferSize;

        // Receive buffer.
        public byte[] Buffer = new byte[BufferSize];

        // Received data string.
        public StringBuilder Sb = new StringBuilder();
    }




    class MainServer
    {

        // TODO change this to singleton rather than using static fields.
        // TODO add method that converts from packet to bytearray.

        public static ManualResetEvent AllDone = new ManualResetEvent(false);

        /// <summary>
        /// The current available identifier for players.
        /// </summary>
        private static int _currentAvailableIdForClients = ServerConstants.PlayerIdPoolStart;

        /// <summary>
        /// List of clients.
        /// </summary>
        private static readonly List<ClientData> MyClients = new List<ClientData>(); // Updated list of clients to serve

        private static RegisteredGames registedGames = new RegisteredGames();

        /// <summary>
        /// gameInfo, gameId
        /// </summary>
        private static List<Tuple<GameInfo, int>> gameInfoWithMaster = new List<Tuple<GameInfo, int>>();

        private static List<Tuple<ulong, ulong>> clientIdsWithGameIds = new List<Tuple<ulong, ulong>>();
        private static readonly Mutex MyMutex = new Mutex();

        /// <summary>
        /// Setups the server.
        /// Must always be called. 
        /// </summary>
        private static void SetupServer()
        {

            Console.WriteLine("Setting up server...");
            // For now, using the console as log

            byte[] bytes = new byte[ServerConstants.ServerBufferSize];


            // TODO - here change according to file found

            IPEndPoint localEndPoint = new IPEndPoint(IPAddress.Any, ServerConstants.UsedPort);

            Socket listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            try
            {
                listener.Bind(localEndPoint);
                listener.Listen(ServerConstants.ListenBacklog);

                while (true)
                {
                    AllDone.Reset();

                    Console.WriteLine("Waiting for connection...");
                    listener.BeginAccept(new AsyncCallback(AcceptConnection), listener);

                    AllDone.WaitOne();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

            Console.WriteLine("\nPress ENTER to continue...");
            Console.Read();
        }

        /// MARK - AUTOMATICALLY CALLED CALLBACKS

        /// <summary>
        /// Accepts the connection.
        /// Gets called when a new client connects to the server
        /// </summary>
        /// <param name="asyncResult">Async result.</param>
        private static void AcceptConnection(IAsyncResult asyncResult)
        {
            // Called when a new connection is established, with an async result.

            // Signal the main thread to continue.
            AllDone.Set();

            // Get the socket that handles the client request.
            Socket listener = (Socket)asyncResult.AsyncState;
            Socket handler = listener.EndAccept(asyncResult);

            ClientData newClient = new ClientData(handler);
            // Create new cliend data entity for further reference.

            MyClients.Add(newClient);

            Console.WriteLine("Client Connected!");

            // Create the state object.
            StateObject state = new StateObject { WorkSocket = handler };


            handler.BeginReceive(state.Buffer, ServerConstants.BufferOffset, StateObject.BufferSize, SocketFlags.None,
                new AsyncCallback(ReceiveMessage), state);

            try
            {
                AllDone.Reset();
                Console.WriteLine("Waiting for another connection...");
                listener.BeginAccept(new AsyncCallback(AcceptConnection), listener);
                AllDone.WaitOne();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        private static Mutex mutextest = new Mutex();
        /// <summary>
        /// Receives the message.
        /// Gets called when a new client sends something to the server
        /// </summary>
        /// <param name="asyncResult">Async result.</param>
        private static void ReceiveMessage(IAsyncResult asyncResult)
        {
            String content = String.Empty;

            // Retrieve the state object and the handler socket
            // from the asynchronous state object.
            StateObject state = (StateObject)asyncResult.AsyncState;
           
                Socket handler = state.WorkSocket;
                int bytesRead = 0;
                try
                {
                    bytesRead = handler.EndReceive(asyncResult);
                }
                catch
                {
                    // client disconnected
                    //TODO remove from clients list
                    return;
                }
                // Read data from the client socket. 

                if (bytesRead > 0)
                {
                    // There  might be more data, so store the data received so far.
                    state.Sb.Append(Encoding.ASCII.GetString(state.Buffer, 0, bytesRead));



                    // Check for end-of-file tag. If it is not there, read 
                    // more data.
                    content = state.Sb.ToString();

                    if (content.Length > 0 && content[content.Length - 1] == ServerConstants.endOfXml)
                    {

                        string[] want = splitMessages(content);
                        foreach (string i in want)
                        {
                            if (i == string.Empty) continue;
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("Read {0} bytes from socket., data = {1} \n",
                                content.Length, i);
                            Console.ResetColor();
                            Object oo = Serialization.stringToObject(i);
                            if (oo != null)
                                HandleRequest(oo, handler);
                            else
                                Console.WriteLine("RECEIVED BUT NOT DESERIALIZED!!");
                    }
                        state.Sb.Clear();
                        try
                        {
                            handler.BeginReceive(state.Buffer, ServerConstants.BufferOffset, StateObject.BufferSize,
                                SocketFlags.None, new AsyncCallback(ReceiveMessage), state);
                        }
                        catch
                        {
                            // client disconnected, socket disposed, cannot call begin receive, everything under controll 
                        }
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Out.WriteLineAsync(content);
                        Console.ResetColor();
                        Console.WriteLine("NOT ALL RECEIVED!!!");

                        handler.BeginReceive(state.Buffer, ServerConstants.BufferOffset, StateObject.BufferSize,
                            SocketFlags.None, new AsyncCallback(ReceiveMessage), state);
                    }

                }
            
            

        }
        private static string[] splitMessages(string par)
        {
            char[] stringSeparators = new char[] { ServerConstants.endOfXml };
            string[] packets = par.Split(stringSeparators, StringSplitOptions.None);
            return packets;
        }
        private static void Send(Socket handler, Object oo)
        {
            string data = Serialization.objectToString(oo);
            // Convert the string data to byte data using ASCII encoding.
            byte[] byteData = Encoding.ASCII.GetBytes(data);
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("sent from server : \n" + data);
            Console.ResetColor();
            try
            {
                // Begin sending the data to the remote device.
                handler.BeginSend(byteData, ServerConstants.BufferOffset, byteData.Length, SocketFlags.None, new AsyncCallback(EndSend), handler);
            }
            catch
            {
                //ignored
            }
        }

        /// <summary>
        /// Callback for ending the send procedure.
        /// </summary>
        /// <param name="asyncResult">Async result.</param>
        private static void EndSend(IAsyncResult asyncResult)
        {
            // Here simply end send on the socket we were transmitting
            Socket senderSocket = (Socket)asyncResult.AsyncState;
            senderSocket.EndSend(asyncResult);
        }


        /// <summary>
        /// assign an id for the client 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="sc"></param>
        /// <returns></returns>
        private static int GiveClientId(ClientType type, Socket sc)
        {
            MyMutex.WaitOne();
            try
            {
                _currentAvailableIdForClients++;
            }
            finally
            {
                MyMutex.ReleaseMutex();
            }

            int allocatedId = _currentAvailableIdForClients;
            foreach (var v in MyClients)
            {
                if (v.Socket != sc) continue;
                v.Id = allocatedId;
                v.ClientType = type;
                break;
            }

            return allocatedId;
        }

        /// <summary>
        /// remove client with id 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="sc"></param>
        /// <returns></returns>
        private static bool RemoveClient(ulong clientId)
        {
            var clientToRemove = MyClients.Find(i => i.Id == (int)clientId);
            clientToRemove.Socket.Close();
            return MyClients.Remove(clientToRemove);
        }

        /// <summary>
        /// get the socket of the given id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private static Socket getSocket(int id)
        {
            return MyClients.Find(i => i.Id == id)?.Socket;
        }

        /// <summary>
        /// called whenever the server receive a message
        /// </summary>
        /// <param name="oo"></param>
        /// <param name="senderSocket"></param>
        private static void HandleRequest(Object oo, Socket senderSocket)
        {
            if (oo is RegisterGame)
            {
                RegisterGameMethod(oo, senderSocket);
            }

            if (oo is GetGames)
            {
                Console.WriteLine("games are sent");
                Send(senderSocket, registedGames); // send back registed games
            }

            if (oo is JoinGame)
            {
                JoinGameMethod(oo, senderSocket);
            }

            if (oo is ConfirmJoiningGame)
            {
                ConfirmJoiningGame cg = (ConfirmJoiningGame)oo;
                Console.WriteLine("cg.playerId = " + cg.playerId);
                clientIdsWithGameIds.Add(new Tuple<ulong, ulong>(cg.playerId, cg.gameId));
                Send(getSocket((int)cg.playerId), cg);
            }

            if (oo is RejectJoiningGame) // send the rejection to the player
            {
                RejectJoiningGame rj = (RejectJoiningGame)oo;
                Send(getSocket((int)rj.playerId), rj);
            }

            if (oo is Game)
            {
                Game g = (Game)oo;
                Console.WriteLine("player id = " + g.playerId);
                Send(getSocket((int)g.playerId), g);
            }

            if (oo is PlayerDisconnected pd)
            {
                Send(getSocket((int)pd.gameMasterId), pd);
                Console.WriteLine(RemoveClient(pd.playerId)
                    ? $"Player with id: {pd.playerId} disconnected from game"
                    : $"Problem occured while disconnecting player {pd.playerId}");
            }

            if (oo is GameMasterDisconnected gmd)
            {
                Console.WriteLine(RemoveClient(gmd.gameId)
                    ? $"Game Master with id: {gmd.gameId} disconnected from game"
                    : $"Problem occured while disconnecting player {gmd.gameId}");
                GameInfo gg = gameInfoWithMaster.Find(item => item.Item2 == (int)gmd.gameId).Item1;
                registedGames.GameInfo.Remove(gg);
                foreach (var player in gmd.Players)
                {
                    Send(getSocket((int)player.id), gmd);
                }
            }

            if (oo is GameStarted)
            {
                GameStarted start = (GameStarted)oo;

                Console.WriteLine("Game started: {0}", start.gameId);

                foreach (var client in clientIdsWithGameIds)
                {
                    if (client.Item2 == start.gameId)
                        Send(getSocket((int)client.Item1), start);
                }
            }


            if (oo is Discover d)
            {
                Console.WriteLine("gameMaster id = " + d.gameId);
                Send(getSocket((int)d.gameId), d);
            }

            if (oo is DiscoverResponse dr)
            {
                Console.WriteLine("player id = " + dr.playerId);
                Socket sc = getSocket((int)dr.playerId);
                if (sc == null)
                    Console.WriteLine("SOCKET IS NULL!!!!!!!!!!!!!!");
                Send(getSocket((int)dr.playerId), dr);
            }
            if (oo is SuggestAction sa)
            {
                Send(getSocket((int)sa.playerId), sa);
            }

            if (oo is SuggestActionResponse sar)
            {
                Send(getSocket((int)sar.playerId), sar);
            }

            if (oo is Move)
            {
                Move move = (Move)oo;
                Console.WriteLine("Move received from player: " + move.playerGuid);
                Send(getSocket((int)move.gameId), move);
            }

            if (oo is Data)
            {
                Data data = (Data)oo;
                Console.WriteLine("Data received from " + data.playerGuid);
                Send(getSocket((int)data.playerId), data);
            }

            if (oo is PlacePiece pp)
            {
                var gameId = pp.gameId;
                Console.WriteLine("PlacePiece send to gameMaster at gameId {0}", gameId);
                Send(getSocket((int)gameId), pp);
            }
            if (oo is PickUpPiece pup)
            {
                Send(getSocket((int)pup.gameId), pup);
            }
            if (oo is TestPiece test)
            {
                Send(getSocket((int)test.gameId), test);
            }
        }

        private static void JoinGameMethod(Object oo, Socket senderSocket)
        {
            JoinGame jg = (JoinGame)oo;
            jg.playerid = (ulong)GiveClientId(ClientType.Agent, senderSocket);
            int gameId = gameInfoWithMaster.Find(match => match.Item1.gameName == jg.gameName).Item2;
            Send(getSocket(gameId), jg);
        }

        /// <summary>
        /// called when game master register its game
        /// </summary>
        /// <param name="oo"></param>
        /// <param name="senderSocket"></param>
        private static void RegisterGameMethod(Object oo, Socket senderSocket)
        {
            int allocatedId = GiveClientId(ClientType.GameMaster, senderSocket);
            RegisterGame rg = (RegisterGame)oo;
            ConfirmGameRegistration c = new ConfirmGameRegistration();
            c.gameId = (ulong)allocatedId;
            registedGames.GameInfo.Add(rg.NewGameInfo);
            gameInfoWithMaster.Add(new Tuple<GameInfo, int>(rg.NewGameInfo, (int)c.gameId));
            Send(senderSocket, c);
        }

        public static void Main(string[] args)
        {
            SetupServer();
            Console.ReadLine();

            // Start server and keep console running
        }
    }
}

