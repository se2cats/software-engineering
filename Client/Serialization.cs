﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using Message.Communication;
namespace Client
{
    public class Serialization
    {


        /// <summary>
        /// Serialize an object into an XML string
        /// </summary>
        /// <typeparam name="T">Type of object to serialize.</typeparam>
        /// <param name="obj">Object to serialize.</param>
        /// <param name="enc">Encoding of the serialized output.</param>
        /// <returns>Serialized (xml) object.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")]
        internal static String SerializeObject<T>(T obj, Encoding enc)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                XmlWriterSettings xmlWriterSettings = new System.Xml.XmlWriterSettings()
                {
                    // If set to true XmlWriter would close MemoryStream automatically and using would then do double dispose
                    // Code analysis does not understand that. That's why there is a suppress message.
                    CloseOutput = false,
                    Encoding = enc,
                    OmitXmlDeclaration = false,
                    Indent = true
                };
                using (System.Xml.XmlWriter xw = System.Xml.XmlWriter.Create(ms, xmlWriterSettings))
                {
                    XmlSerializer s = new XmlSerializer(typeof(T));
                    s.Serialize(xw, obj);
                }
                return enc.GetString(ms.ToArray());
            }
        }

        public static XmlSerializer RegisterGameSerializer = new XmlSerializer(typeof(RegisterGame));
        public static XmlSerializer ConfirmGameRegistrationSerializer = new XmlSerializer(typeof(ConfirmGameRegistration));
        public static XmlSerializer GetGamesSerializer = new XmlSerializer(typeof(GetGames));
        public static XmlSerializer JoinGameSerializer = new XmlSerializer(typeof(JoinGame));
        public static XmlSerializer ConfirmJoiningGameSerializer = new XmlSerializer(typeof(ConfirmJoiningGame));
        public static XmlSerializer RegisteredGamesSerializer = new XmlSerializer(typeof(RegisteredGames));
        public static XmlSerializer RejectJoiningGameSerializer = new XmlSerializer(typeof(RejectJoiningGame));
        public static XmlSerializer GameSerializer = new XmlSerializer(typeof(Game));
        public static XmlSerializer PlayerDisconnectedSerializer = new XmlSerializer(typeof(PlayerDisconnected));
        public static XmlSerializer GameMasterDisconnectedSerializer = new XmlSerializer(typeof(GameMasterDisconnected));
        public static XmlSerializer SuggestActionSerializer = new XmlSerializer(typeof(SuggestAction));
        public static XmlSerializer SuggestActionResponseSerializer = new XmlSerializer(typeof(SuggestActionResponse));
        public static XmlSerializer GameStartedSerializer = new XmlSerializer(typeof(GameStarted));
        public static XmlSerializer MoveSerializer = new XmlSerializer(typeof(Move));
        public static XmlSerializer DataSerializer = new XmlSerializer(typeof(Data));
        public static XmlSerializer PlacePieceSerializer = new XmlSerializer(typeof(PlacePiece));
        public static XmlSerializer PickupPieceSerializer = new XmlSerializer(typeof(PickUpPiece));
        public static XmlSerializer TestPieceSerializer = new XmlSerializer(typeof(TestPiece));
        public static XmlSerializer DiscoverSerializer = new XmlSerializer(typeof(Discover));
        public static XmlSerializer DiscoverResponseSerializer = new XmlSerializer(typeof(DiscoverResponse));
        // TODO add all other serializers




        /// <summary>
        /// Serialize an object into a string
        /// </summary>
        /// <param name="par"></param>
        /// <returns></returns>
        public static string objectToString(Object par)
        {
            StringWriter sw = new StringWriter();

            string now = "";
            if (par is RegisterGame)
            {
                now = SerializeObject<RegisterGame>(par as RegisterGame, Encoding.UTF8);
            }
            if (par is ConfirmGameRegistration)
            {
                now = SerializeObject<ConfirmGameRegistration>(par as ConfirmGameRegistration, Encoding.UTF8);
            }
            if (par is GetGames)
            {
                now = SerializeObject<GetGames>(par as GetGames, Encoding.UTF8);
            }
            if (par is JoinGame)
            {
                now = SerializeObject<JoinGame>(par as JoinGame, Encoding.UTF8);
                now = now.Replace("playerid", "playerId");  // necessery
            }
            if (par is ConfirmJoiningGame)
            {
                now = SerializeObject<ConfirmJoiningGame>(par as ConfirmJoiningGame, Encoding.UTF8);
            }
            if (par is RegisteredGames)
            {
                now = SerializeObject<RegisteredGames>(par as RegisteredGames, Encoding.UTF8);
            }
            if (par is RejectJoiningGame)
            {
                now = SerializeObject<RejectJoiningGame>(par as RejectJoiningGame, Encoding.UTF8);
            }
            if (par is Game)
            {
                now = SerializeObject<Game>(par as Game, Encoding.UTF8);
            }
            if (par is PlayerDisconnected)
            {
                now = SerializeObject<PlayerDisconnected>(par as PlayerDisconnected, Encoding.UTF8);
            }
            if (par is GameMasterDisconnected)
            {
                now = SerializeObject<GameMasterDisconnected>(par as GameMasterDisconnected, Encoding.UTF8);
            }
            if (par is SuggestAction)
            {
                now = SerializeObject<SuggestAction>(par as SuggestAction, Encoding.UTF8);
            }
            if (par is SuggestActionResponse)
            {
                now = SerializeObject<SuggestActionResponse>(par as SuggestActionResponse, Encoding.UTF8);
            }
            if(par is PlacePiece)
            {
               now = SerializeObject<PlacePiece>(par as PlacePiece, Encoding.UTF8);
            }
            if (par is GameStarted)
            {
                now = SerializeObject<GameStarted>(par as GameStarted, Encoding.UTF8);
            }

            if (par is Move)
            {
                now= SerializeObject<Move>(par as Move, Encoding.UTF8); 
            }
            if (par is Data)
            {
                now = SerializeObject<Data>(par as Data, Encoding.UTF8);
                Serialization.DataSerializer.Serialize(sw,par);
            }
            // TODO add all other types the client may send
            if (par is Discover)
            {
                now = SerializeObject<Discover>(par as Discover, Encoding.UTF8);
            }
            if (par is DiscoverResponse)
            {
                //DiscoverResponseSerializer.Serialize(sw, par);
                now = SerializeObject<DiscoverResponse>(par as DiscoverResponse, Encoding.UTF8);
            }
            if(par is PickUpPiece)
            {
                now = SerializeObject<PickUpPiece>(par as PickUpPiece, Encoding.UTF8);
                
            }
            if(par is TestPiece)
            {
                now = SerializeObject<TestPiece>(par as TestPiece,Encoding.UTF8);
            }

            // TODO add all other types the client may send

            if (now.Length > 0)
            {
                // the first character is question mark so remove it
                return now.Substring(1) + Server.ServerConstants.endOfXml;
            }
            else
                return "";
        }

        /// <summary>
        /// Deserialize a string into a proper object
        /// </summary>
        /// <param name="par"></param>
        /// <returns></returns>
        public static Object stringToObject(string par)
        {
            StringReader sr = new StringReader(par);
            XmlReader reader = new XmlTextReader(sr);
            try
            {
                if (ConfirmGameRegistrationSerializer.CanDeserialize(reader))
                {
                    return ConfirmGameRegistrationSerializer.Deserialize(new StringReader(par));
                }
                else if (RegisterGameSerializer.CanDeserialize(reader))
                {
                    return RegisterGameSerializer.Deserialize(new StringReader(par));
                }
                else if (GetGamesSerializer.CanDeserialize(reader))
                {
                    return GetGamesSerializer.Deserialize(new StringReader(par));
                }
                else if (JoinGameSerializer.CanDeserialize(reader))
                {
                    par = par.Replace("playerId", "playerid");  // necessery
                    return JoinGameSerializer.Deserialize(new StringReader(par));
                }
                else if (ConfirmJoiningGameSerializer.CanDeserialize(reader))
                {
                    return ConfirmJoiningGameSerializer.Deserialize(new StringReader(par));
                }
                else if (RegisteredGamesSerializer.CanDeserialize(reader))
                {
                    return RegisteredGamesSerializer.Deserialize(new StringReader(par));
                }
                else if (RejectJoiningGameSerializer.CanDeserialize(reader))
                {
                    return RejectJoiningGameSerializer.Deserialize(new StringReader(par));
                }
                else if (GameSerializer.CanDeserialize(reader))
                {
                    return GameSerializer.Deserialize(new StringReader(par));
                }
                else if (PlayerDisconnectedSerializer.CanDeserialize(reader))
                {
                    return PlayerDisconnectedSerializer.Deserialize(new StringReader(par));
                }
                else if (GameMasterDisconnectedSerializer.CanDeserialize(reader))
                {
                    return GameMasterDisconnectedSerializer.Deserialize(new StringReader(par));
                }
                else if (GameStartedSerializer.CanDeserialize(reader))
                {
                    return GameStartedSerializer.Deserialize(new StringReader(par));
                }
                else if (DiscoverSerializer.CanDeserialize(reader))
                {
                    return DiscoverSerializer.Deserialize(new StringReader(par));
                }
                else if (DiscoverResponseSerializer.CanDeserialize(reader))
                {
                    return DiscoverResponseSerializer.Deserialize(new StringReader(par));
                }
                else if (SuggestActionSerializer.CanDeserialize(reader))
                {
                    return SuggestActionSerializer.Deserialize(new StringReader(par));
                }
                else if (SuggestActionResponseSerializer.CanDeserialize(reader))
                {
                    return SuggestActionResponseSerializer.Deserialize(new StringReader(par));
                }
                else if (MoveSerializer.CanDeserialize(reader))
                {
                    return MoveSerializer.Deserialize(new StringReader(par));
                }
                else if (PlacePieceSerializer.CanDeserialize(reader))
                {
                    return PlacePieceSerializer.Deserialize(new StringReader(par));
                }
                else if (PickupPieceSerializer.CanDeserialize(reader))
                {
                    return PickupPieceSerializer.Deserialize(new StringReader(par));
                }
                else if (TestPieceSerializer.CanDeserialize(reader))
                {
                    return TestPieceSerializer.Deserialize(new StringReader(par));
                }
                else if (DataSerializer.CanDeserialize(reader))
                {
                    return DataSerializer.Deserialize(new StringReader(par));
                }
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(e.ToString());
                Console.ResetColor();
                return null;
            }
            //Console.ForegroundColor = ConsoleColor.Red;
            //Console.WriteLine("\nxml message = " + par + "\n");
            //Console.ResetColor();
            // TODO add all other types the client may receive
            //throw new FieldAccessException("in second there is no serializer for that type of message!, add it in Serialization class");
            return null;
        }

    }
}
