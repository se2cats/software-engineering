﻿namespace Client
{
    public enum FieldContent
    {
        Empty, Item, FakeItem, Player, GoalTask
    }
    
    public enum FieldType
    {
        GoalArea, Board
    }
    public class Field
    {
        public FieldContent Content { get; set; }
        public FieldType Type { get; set; }
        public Message.Communication.GoalFieldType goalFieldType { get; set; }       
        public int PlayerID { get; set; }

        public Field()
        {
            Content = FieldContent.Empty;
            Type = FieldType.Board;
            goalFieldType = Message.Communication.GoalFieldType.unknown;
        }

    }
    public class BoardDimensions
    {
        public BoardDimensions(int width, int height, int goalAreaHeight)
        {
            Width = width;
            Height = height;
            GoalAreaHeight = goalAreaHeight;
        }

        public int Width { get; set; }
        public int Height { get; set; }
        public int GoalAreaHeight { get; set; }

    }
    public class BoardGame
    {
        public Field[,] Board = new Field[1,1];
        /// <summary>
        /// Distance[i, j] contains the manhattan distance to the nearest item for the player board
        /// The player update its own Distance array after discovering neighbours
        /// </summary>
        public int[,] Distance = new int[1, 1];
        public BoardDimensions BoardDim = new BoardDimensions(1,1,1);

        public int Width { get; set; }
        public int Height { get; set; }
        public int GoalAreaHeight { get; set; }

        public BoardGame()
        { }
        public BoardGame(int width, int height, int goalAreaHeight)
        {
            Width = width;
            Height = height;
            GoalAreaHeight = goalAreaHeight;
            BoardDim =new BoardDimensions(Width,Height,GoalAreaHeight);
            Board = new Field[width,height];
            Distance = new int[width, height];
            for (int i = 0; i < Width; i++)
            {
                for (int j = 0; j < Height; j++)
                {
                    Board[i, j] = new Field();
                    Board[i, j].Content = FieldContent.Empty;
                    Distance[i, j] = int.MaxValue;

                    if (j <GoalAreaHeight || j >= (Height - GoalAreaHeight))
                        Board[i, j].Type = FieldType.GoalArea;
                }
            }
        }

        //public bool IsOccupied(int xCoordinate, int yCoordinate)
        //{
        //    if (Board[xCoordinate, yCoordinate].Content != FieldContent.Player)
        //        return false;
        //    else return true;
        //}

        //public bool ContainsItem(int xCoordinate, int yCoordinate)
        //{
        //    if (Board[xCoordinate, yCoordinate].Content.Equals(FieldContent.Item))
        //    {
        //        return true;
        //    }
        //    else return false;
        //}

        public bool InRange(int x, int y)
        {
            return x >= 0 && x < Width && y >= 0 && y < Height;
        }

        public bool inGoalArea(int x, int y)
        {
            return Board[x, y].Type == FieldType.GoalArea;
        }
        public bool BlueGoalArea(int x, int y)
        {
            return Board[x, y].Type == FieldType.GoalArea && y < GoalAreaHeight;
        }
        public bool RedGoalArea(int x, int y)
        {
            return Board[x, y].Type == FieldType.GoalArea && y >= Height - GoalAreaHeight;
        }
    }
}