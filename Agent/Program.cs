﻿using Server;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Controls;
using Board;
using Message.Communication;
using System.Xml.Serialization;
using System.IO;

namespace Agent
{
    class Program
    {
       
        static void Main(string[] args)
        {
            // var player = new Player(gameName, color == "red" ? TeamColour.red : TeamColour.blue, role == "leader" ? PlayerRole.leader : PlayerRole.member);

            Player player;
            if (args.Length == 0)
                player = new Player();
            else
                player = new Player(args[0]);
            Console.ReadKey();
        }
    }
}
