﻿using System;
using System.IO;
using System.Runtime.CompilerServices;
using Server;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using Board;
using System.Windows.Media;
using Client;
using Message.Communication;
namespace Agent
{
    public class Team
    {
        public Team()
        {
        }

        public List<int> PlayersIds = new List<int>();
    }
    public class Player : Client.Client
    {
        public Team MyTeam = new Team();
        private Location location = null;
        private int teamLeaderId;
        private string playerGuid;
        private int gameMasterId;
        private string gameName = "";
        private TeamColour color;
        private PlayerRole role;
        private Piece piece;
        private int playerId = 0;
        bool holdsItem = false;
        bool waitingForMoveResponse = false;
        ManualResetEvent waitForId = new ManualResetEvent(false);
        private bool requestedToFinish = false;

        public Piece Piece { get => piece; set => piece = value; }
        public Location goalFieldToGo = new Location();
        public Object monitor = new Object();
        public bool waitngForTestResponse = false;
        public Player(string gameName, TeamColour color, PlayerRole role)
        {
            SendPacket(new GetGames());


            this.gameName = gameName;
            this.color = color;
            this.role = role;
            this.piece = new Piece() { type = PieceType.unknown };
            waitForId.WaitOne();
            waitForId.Reset();
        }

        public Player()
        {
            SendPacket(new GetGames());
            waitForId.WaitOne();
            waitForId.Reset();

            //Action();
        }
        public Player(string gameName)
        {
            this.gameName = gameName;
            SendPacket(new GetGames());
        }



        /// <summary>
        /// get unknown goal field to go to
        /// </summary>
        /// <returns></returns>
        private Location getUnknownGoalField()
        {
            List<Location> fields = new List<Location>();

            for (int i = 0; i < Board.Width; ++i)
            {
                if (color == TeamColour.blue)
                {
                    for (int j = 0; j < Board.GoalAreaHeight; ++j)
                    {
                        if (Board.Board[i, j].goalFieldType == GoalFieldType.unknown)
                            fields.Add(new Location() { x = (uint)i, y = (uint)j });
                    }
                }
                else
                {
                    for (int j = Board.Height - Board.GoalAreaHeight; j < Board.Height; ++j)
                    {
                        if (Board.Board[i, j].goalFieldType == GoalFieldType.unknown)
                            fields.Add(new Location() { x = (uint)i, y = (uint)j });
                    }
                }
            }
            Location ret = fields[new Random().Next(fields.Count)]; // choose randomly so that not all players go to the same field
            return ret;
        }
        /// <summary>
        /// The player go to the nearest piece if it has no piece
        /// it goes to goalFieldToGo if it has a piece
        /// </summary>
        /// <returns></returns>
        public void SendMove()
        {
            if (waitingForMoveResponse) return;

            Move move = new Move
            {
                playerGuid = playerGuid,
                gameId = (ulong)gameMasterId,
                directionSpecified = true


            };
            int[] dx = { -1, 0, 1, 0 };
            int[] dy = { 0, 1, 0, -1 };
            if (piece != null) // holding a piece
            {
                bool send = false;
                if (location.x > goalFieldToGo.x && !blocked)    // go left
                {
                    move.direction = MoveType.left;
                    Console.WriteLine("Trying to move left");
                    send = true;
                }
                else if (location.x < goalFieldToGo.x && !blocked)
                {
                    move.direction = MoveType.right;
                    Console.WriteLine("Trying to move right");
                    send = true;
                }
                else if (location.y < goalFieldToGo.y && !blocked)
                {
                    move.direction = MoveType.up;
                    Console.WriteLine("Trying to move up");
                    send = true;
                }
                else if (location.y > goalFieldToGo.y && !blocked)
                {
                    move.direction = MoveType.down;
                    Console.WriteLine("Trying to move down");
                    send = true;
                }
                else
                {
                    Console.WriteLine("Unblocking");
                    Array directions = Enum.GetValues(typeof(MoveType));
                    Random r = new Random();
                    move.direction = (MoveType)directions.GetValue(r.Next(directions.Length));
                    Console.WriteLine("Trying to move " + move.direction);
                    send = true;

                }
                if (send)
                {
                    SendPacket(move);
                    waitingForMoveResponse = true;

                }
            }
            else
            {

                if (Board.inGoalArea((int)location.x, (int)location.y))
                {
                    if (location.y > Board.GoalAreaHeight)   // go down heading to the task area
                    {
                        move.direction = MoveType.down;
                        Console.WriteLine("Trying to move down");
                    }
                    else
                    {
                        move.direction = MoveType.up;
                        Console.WriteLine("Trying to move up");
                    }
                }
                else
                {
                    int min = int.MaxValue;
                    Tuple<int, int> testLocation = new Tuple<int, int>((int)location.x, (int)location.y);
                    for (int i = 0; i < 4; i++)
                    {
                        int x = (int)location.x + dx[i];
                        int y = (int)location.y + dy[i];
                        if (Board.InRange(x, y))
                        {
                            if (Board.Distance[x, y] <= min)
                            {
                                min = Board.Distance[x, y];
                                testLocation = new Tuple<int, int>(x, y);
                                if (dx[i] == -1)
                                {
                                    move.direction = MoveType.left;
                                }

                                if (dx[i] == 1)
                                {
                                    move.direction = MoveType.right;
                                }

                                if (dy[i] == -1)
                                {
                                    move.direction = MoveType.down;
                                }

                                if (dy[i] == 1)
                                {
                                    move.direction = MoveType.up;
                                }
                            }
                        }
                    }
                }
                waitingForMoveResponse = true;
                if (move.direction == MoveType.up)
                    Console.WriteLine("Trying to move up");
                else if (move.direction == MoveType.down)
                    Console.WriteLine("Trying to move down");
                else if (move.direction == MoveType.left)
                    Console.WriteLine("Trying to move left");
                else
                    Console.WriteLine("Trying to move right");
                SendPacket(move);
            }

        }

        public void HandleMoveRepsponse(Data data)
        {
            if (location.x == data.PlayerLocation.x && location.y == data.PlayerLocation.y)
            {
                blocked = true;
                Console.WriteLine("I got blocked :(");
            }
            else blocked = false;
                
            location = data.PlayerLocation;
            waitingForMoveResponse = false;
            if (piece != null && goalFieldToGo != null && goalFieldToGo.x == location.x && goalFieldToGo.y == location.y)   // I am on goal field holding a piece so place it
            {
                PlacePiece placePiece = new PlacePiece()
                {
                    gameId = (ulong)gameMasterId,
                    playerGuid = playerGuid
                };
                SendPacket(placePiece);
                piece = null;
                goalFieldToGo = null;
                Console.WriteLine("PLACE A PIECE ON " + location.x + " " + location.y);
            }
            Console.Out.WriteLineAsync("Move confirmation received " + data.PlayerLocation.x.ToString() + " " + data.PlayerLocation.y.ToString() + (piece != null ? " HAS ITEM" : ""));
        }

        public bool blocked { get; set; }


        async Task<string> AsyncAction()
        {
            bool gameFinished = requestedToFinish;
            int c = 0;
            while (!requestedToFinish)
            {
                if (piece != null && piece.type == PieceType.unknown && !waitngForTestResponse)
                {
                    waitngForTestResponse = true;
                    Console.WriteLine("PIECE IS UNKNOWN");
                    TestPiece testPiece = new TestPiece()
                    {
                        gameId = (ulong)gameMasterId,
                        playerGuid = playerGuid
                    };
                    SendPacket(testPiece);
                }
                else
                {

                    if (c % 4 == 0)
                    {
                        Discover();
                    }
                    else
                        SendMove();
                    c++;
                    c %= 4;
                    //swith for actions which do not depend on the piece type like mve, discover and others
                    //Random r = new Random();
                    //int action = r.Next(2);  //number of actions+1, currently only discover and move

                    //switch (action)
                    //{
                    //    case 0:
                    //        Discover();
                    //        break;
                    //    case 1:
                    //        SendMove();
                    //        break;
                    //}
                }

                gameFinished = requestedToFinish; // if it worked, make as succeeded, else retry
                await Task.Delay(200); // arbitrary delay
            }
            return "Move sent";
        }

        public void Discover()
        {
            if (waitingForMoveResponse)
                return;
            var d = new Discover
            {
                gameId = (ulong)gameMasterId,
                playerId = (ulong)Id,
                playerGuid = playerGuid
            };
            SendPacket(d);
            //  Thread.Sleep(1500);
        }

        public override void HandleReceivePacket(object oo)
        {
            lock (monitor)
            {
                // TODO check the types of objects the players may receive, e.g (Discover, Move...)

                if (oo is RegisteredGames)
                {
                    RegisteredGamesMethod(oo);

                }
                else if (oo is RejectJoiningGame)
                {
                    Console.WriteLine("Rejected!, no spots available in that game");
                }
                else if (oo is ConfirmJoiningGame)
                {
                    ConfirmJoiningGameMethod(oo);
                }
                else if (oo is Game)
                {
                    GameMethod(oo);
                    StartGame();
                }
                else if (oo is GameMasterDisconnected)
                {
                    requestedToFinish = true;
                    Console.WriteLine("Game master disconnected");

                }
                else if (oo is SuggestAction sa)
                {
                    Console.WriteLine("recieved suggestion");
                    handleSuggestAction(sa);
                }
                else if (oo is SuggestActionResponse)
                {
                    Console.WriteLine("recieved suggestion response ");

                }
                else if (oo is GameStarted)
                {
                    StartGame();
                }
                else if (oo is Move move)
                {
                    Console.WriteLine("Move received");
                }

                else if (oo is DiscoverResponse dr)
                {
                    if (dr.gameFinished)
                    {
                        requestedToFinish = true;
                    }

                    var tfList = dr.TaskFields.ToList();
                    bool noPieceLeft = false;
                    foreach (var i in tfList)
                    {
                        if (i == null) continue;
                        Board.Distance[i.x, i.y] = i.distanceToPiece;
                        if (i.distanceToPiece == -1)
                            noPieceLeft = true;
                    }
                    if (noPieceLeft)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("NO PIECES LEFT!!");
                        Console.ResetColor();
                    }
                }
                else if (oo is PickUpPiece)
                {
                    Console.WriteLine("Picking up a piece");
                    handlePiecePickUpAction();
                }
                else if (oo is PlacePiece)
                {
                    Console.WriteLine("Placing down a piece");
                    handlePlacePieceAction();
                }
                else if (oo is Data data)
                {
                    HandleDate(data);
                }
            }
        }

        private void HandlePlacePieceResponse(Data data)
        {
            foreach (var i in data.GoalFields)
            {
                if (i == null) continue;
                if (i.type != GoalFieldType.unknown)
                {
                    Board.Board[(int)i.x, (int)i.y].goalFieldType = i.type;
                    if (location.x == i.x && location.y == i.y && i.type == GoalFieldType.goal
                    ) // response to place piece message
                    {
                        Board.Board[(int)i.x, (int)i.y].goalFieldType = i.type;
                        Console.ForegroundColor = ConsoleColor.DarkYellow;
                        Console.WriteLine("COMPLETED A TASK!!");
                        Console.ResetColor();
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.Cyan;
                        Console.WriteLine("NON GOAL TASK!!");
                        Console.ResetColor();
                    }
                }
            }
        }
        private void HandleDate(Data data)
        {
            requestedToFinish = data.gameFinished;
            if (data.PlayerLocation != null)
                HandleMoveRepsponse(data);
            //
            if (data.GoalFields != null)
            {
                HandlePlacePieceResponse(data);
            }

            if (data.Pieces != null)
            {
                foreach (var i in data.Pieces)
                {
                    if (i == null) continue;
                    if ((int)i.playerId == Id && i.type == PieceType.unknown)   // responce to pick up piece
                    {
                        piece = i;          // if sham send destroy
                        goalFieldToGo = getUnknownGoalField();
                        waitngForTestResponse = true;
                        Console.WriteLine("PICKED UP A PIECE!!! GOING TO " + goalFieldToGo.x + " " + goalFieldToGo.y);
                        break;
                    }
                    else if ((int)i.playerId == Id)
                    {
                        Console.WriteLine("PIECE IS " + (i.type == PieceType.normal ? "NORMAL" : "SHAM"));
                        piece.type = i.type;
                    }
                }
            }

            if (data.TaskFields != null)
            {
                foreach (var task in data.TaskFields)
                {
                    if (task == null) continue;
                    if (task.x == location.x && task.y == location.y && task.distanceToPiece == 0 && piece == null) // found piece, pick it if not holding one
                    {
                        PickUpPiece pickUp = new PickUpPiece()
                        {
                            gameId = (ulong)gameMasterId,
                            playerGuid = playerGuid
                        };
                        SendPacket(pickUp);
                        break;
                    }
                }

                // discover response
                var tfList = data.TaskFields.ToList();
                bool noPieceLeft = false;
                foreach (var i in tfList)
                {
                    if (i == null) continue;
                    Board.Distance[i.x, i.y] = i.distanceToPiece;
                    if (i.distanceToPiece == -1)
                        noPieceLeft = true;
                }
                if (noPieceLeft)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("NO PIECES LEFT!!");
                    Console.ResetColor();
                }
            }


        }
        private void StartGame()
        {
            Console.WriteLine("Game started");
            //SendMove();
            //Action();

            Task<string> playerAction = AsyncAction();
            playerAction.ContinueWith(t => { Console.WriteLine("PlayerAction finished"); });
        }


        private void suggestAction()
        {
            var TF = new TaskField
            {
                distanceToPiece = 2,
                pieceId = 1,
                pieceIdSpecified = false,
                playerId = (ulong)playerId,
                playerIdSpecified = true,
                timestamp = DateTime.Now,
                x = 2,
                y = 1
            };
            var GF = new GoalField
            {
                playerId = (ulong)playerId,
                playerIdSpecified = true,
                team = color,
                timestamp = DateTime.Now,
                type = GoalFieldType.goal,
                x = 1,
                y = 4
            };
            var sa = new SuggestAction
            {
                TaskFields = new[] { TF, TF, TF },
                GoalFields = new[] { GF, GF, GF },
                playerId = (ulong)MyTeam.PlayersIds.First(),
                senderPlayerId = (ulong)playerId,
                gameId = (ulong)gameMasterId
            };

            SendPacket(sa);
        }

        private void handleSuggestAction(SuggestAction sa)
        {
            //decision process
            var r = new Random();

            var TF = new TaskField();
            TF.distanceToPiece = 2;
            TF.pieceId = 1;
            TF.pieceIdSpecified = false;
            TF.playerId = (ulong)playerId;
            TF.playerIdSpecified = true;
            TF.timestamp = DateTime.Now;
            TF.x = 1;
            TF.y = 2;
            var GF = new GoalField();
            GF.playerId = (ulong)playerId;
            GF.playerIdSpecified = true;
            GF.team = color;
            GF.timestamp = DateTime.Now;
            GF.type = GoalFieldType.goal;
            GF.x = 3;
            GF.y = 1;
            var sar = new SuggestActionResponse
            {
                TaskFields = new[] { TF, TF, TF },
                GoalFields = new[] { GF, GF, GF },
                playerId = sa.senderPlayerId,
                senderPlayerId = (ulong)playerId,
                gameId = (ulong)gameMasterId
            };

            SendPacket(sar);
        }
        private void handlePlacePieceAction()
        {
            PlacePiece placePiece = new PlacePiece
            {
                gameId = (ulong)gameMasterId,
                playerGuid = playerGuid
            };
            holdsItem = false;
            SendPacket(placePiece);
        }
        private void handlePiecePickUpAction()
        {
            PickUpPiece pickUp = new PickUpPiece
            {
                playerGuid = playerGuid,
                gameId = (ulong)gameMasterId
            };
            holdsItem = true;
            SendPacket(pickUp);
        }
        private void GameMethod(Object oo)
        {
            Game g = (Game)oo;

            // add the team
            foreach (Message.Communication.Player p in g.Players)
            {
                if ((int)p.id == GetId()) continue;

                MyTeam.PlayersIds.Add((int)p.id);

            }
            location = new Location();
            location.x = g.PlayerLocation.x;
            location.y = g.PlayerLocation.y;

            Board = new BoardGame((int)g.Board.width, 2 * (int)g.Board.goalsHeight + (int)g.Board.tasksHeight, (int)g.Board.goalsHeight);

            // TODO update the board
        }
        private void ConfirmJoiningGameMethod(Object oo)
        {
            ConfirmJoiningGame cg = (ConfirmJoiningGame)oo;
            color = cg.PlayerDefinition.team;
            role = cg.PlayerDefinition.role;
            playerGuid = cg.privateGuid;
            playerId = (int)cg.playerId;
            playerGuid = cg.privateGuid;
            gameMasterId = (int)cg.gameId;
            SetId((int)cg.playerId);

            Console.WriteLine("Player joined game " + gameName + " with id = " + playerId + ", color = " + color + ", role = " + role);
            waitForId.Set();
        }
        private void RegisteredGamesMethod(Object oo)
        {
            RegisteredGames rg = (RegisteredGames)oo;
            if (rg.GameInfo.Count == 0)
            {
                Console.WriteLine("no games!");
                Thread.Sleep(1000);  // wait a little then send a request again
                SendPacket(new GetGames());
            }
            else
            {
                Console.WriteLine("registered games:");
                foreach (var gameInfo in rg.GameInfo)
                {
                    Console.WriteLine($"game name: {gameInfo.gameName}, places in red: {gameInfo.redTeamPlayers}, places in blue: {gameInfo.blueTeamPlayers}");
                }

                if (gameName == string.Empty)
                {
                    string name = "";
                    do
                    {
                        Console.Write("Name of the game to join: ");
                        name = Console.ReadLine();
                    } while (rg.GameInfo.All(gg => gg.gameName != name));

                    Console.Write("Preferred color : ");
                    if (!Enum.TryParse(Console.ReadLine(), out TeamColour preferredTeam))
                    {
                        preferredTeam = TeamColour.blue;
                    }

                    Console.Write("Preferred role : ");
                    if (!Enum.TryParse(Console.ReadLine(), out PlayerRole preferredRole))
                    {
                        preferredRole = PlayerRole.member;
                    }
                    var jg = new JoinGame
                    {
                        gameName = name,
                        preferredTeam = preferredTeam,
                        preferredRole = preferredRole,
                        playerIdSpecified = false
                    };

                    SendPacket(jg);
                }
                else
                {
                    var jg = new JoinGame
                    {
                        gameName = gameName,
                        preferredTeam = TeamColour.blue,
                        preferredRole = PlayerRole.leader,
                        playerIdSpecified = false
                    };

                    SendPacket(jg);
                }



            }
        }

        public void SendDisconnect()
        {
            handlePlacePieceAction();
            Thread.Sleep(5000);
            handlePiecePickUpAction();
            Thread.Sleep(5000);
            suggestAction();
            Thread.Sleep(5000);

            PlayerDisconnected pg = new PlayerDisconnected { playerId = (ulong)playerId, gameMasterId = (ulong)gameMasterId };


            SendPacket(pg);

            Console.WriteLine("Player is terminating");
            Thread.Sleep(1000);

        }
    }
}
