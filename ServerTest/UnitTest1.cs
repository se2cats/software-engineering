﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Agent;
using System.Diagnostics;
using Server;
using System.Threading;
namespace ServerTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            ProcessStartInfo server = new ProcessStartInfo("Server.exe");
            
            // <configuration>
            int bluePlayers = 2, redPlayers = 2;
            int normalPieces = 20;
            int shams = 0;
            int goalTasks = 4;
            int width = 10;
            int heigh = 10;
            int goalAreaHeight = 2;
            string gameName = "easy";
            // </configuration>

            ProcessStartInfo master = new ProcessStartInfo("GameMaster.exe", bluePlayers.ToString() + " " + redPlayers.ToString() + " " + gameName + " " + 
                normalPieces.ToString() + " " +  shams + " " + goalTasks.ToString() + " " + width.ToString() + " " + 
                heigh.ToString() + " " + goalAreaHeight.ToString());

            int size = redPlayers + bluePlayers;
            ProcessStartInfo[] p = new ProcessStartInfo[size];
            for (int i = 0; i < size; i++)
            {
                p[i] = new ProcessStartInfo("Agent.exe", gameName + " red leader");
            }
            Process.Start(server);
            Thread.Sleep(2000);
            Process.Start(master);
            Thread.Sleep(3000);
            for (int i = 0; i < size; i++)
            {
                Process.Start(p[i]);
            }
            //Process.Start(agent1);
            //Process.Start(agent2);

            /*
            ProcessStartInfo info = new ProcessStartInfo("Server.exe");

            Process exeProcess = Process.Start(info);
            exeProcess.WaitForExit(2000);
            Process[] p = new Process[65];
            for (int i = 0; i < 65; ++i)
            {
                ProcessStartInfo startInfo = new ProcessStartInfo("Agent.exe", "whatEver red leader");
                startInfo.CreateNoWindow = false;
                startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                Process exe = Process.Start(startInfo);
                p[i] = exe;
            }
            Thread.Sleep(10000);
            exeProcess.Kill();
            for (int i = 0; i < 65; ++i)
            {
                p[i].Kill();
            }*/
        }
    }
}
